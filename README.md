## Abtract ##

This shell script is intended to convert audio files, from various format, to the ogg one.

The filename extension set the input format.

It requires the following packages to work correctly:

*  `sox`, `mp3|wav` 
* `musepack`, `mpc`
* `m4a`, `ffmpeg` 
* last, for any other extension, `mplayer`.

## Usage ##

```sh
audiogg [-c] filename
Use the -c flag to cleanup the input files if the conversion succeeded.
```

one may use `find` to convert a full bunch of files:

```
find . -iname "*.mp3" -exec audiogg -c  "{}" \;
```
