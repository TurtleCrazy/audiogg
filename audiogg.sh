#!/bin/sh

usage() {
	echo 'Usage:' "${0}"' [-c] filename '
	echo '-c        clean old files'
	echo '-------------------------'
}

parse()
{
	[ "${1}" == "-c" ] && REMOVE=1 && shift
	fullname="${@}"

}

if [ $# -lt 1 ] 
then 
	usage ${*}
	exit 0
fi

REMOVE=0
parse ${*} 

# regular files only
echo $fullname
[ ! -r "${fullname}" ] && usage && exit 1


EXT="${fullname##*.}"
fname="${fullname%.*}"

O=${fname}.ogg
# temporary ouput
O2=${fname}.wav

echo ".${EXT}" : converting "${fullname}" to "${O}" 

case ${EXT} in
	[wW][aA][vV])
		sox -t wav "${fullname}" "${O}"
		;;
	[mM][pP]3)
		sox -t mp3 "${fullname}" "${O}"
		;;
	[mM]4[aA])
		ffmpeg -i "${fullname}" "${O}"
		;;
	[mM][pP][cC])
		mpcdec  "${fullname}" "${O2}" && sox "${O2}" "${O}"
		[ -w "${O2}" ] && rm -- "${O2}"
		;;
	*) 
		mplayer -ao pcm:fast:file="${O2}" -vo null -vc null "${fullname}" && sox "${O2}" "${O}" 
		[ -w "${O2}" ] && rm -- "${O2}"
		;;
esac

[ ${?} -ne 0 ] && echo failure && exit 1

[ ${REMOVE} -ne 1 ] && exit 0

[ -r "${O}" -a -f "${fullname}" ] && rm -- "${fullname}"





